package agenda.controller;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.validator.ActivityValidator;
import agenda.model.validator.ContactValidator;
import agenda.model.validator.UserValidator;
import agenda.repository.interfaces.IRepositoryActivity;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import agenda.service.ActivityService;
import agenda.service.ContactService;
import agenda.service.UserService;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by elisei on 12.03.2018.
 */
public class Controller {

    private final ContactService contactService;
    private final ActivityService activityService;
    private final UserService userService;

    public Controller(ContactService contactService, ActivityService activityService, UserService userService){
        this.activityService = activityService;
        this.contactService = contactService;
        this.userService = userService;
    }


    public boolean canLogIN(String username, String password) {
        return userService.canLogIn(username, password);
    }

    public void addContact(String name, String address, String telefon, String email, User user) throws InvalidFormatException, RepositoryException {
        contactService.addContact(name, address, telefon, email, user);
    }

    public void addActivity(User user, String name, Date start, Date end, LinkedList<Contact> contacts, String description, String location) throws InvalidFormatException, RepositoryException {
        activityService.addActivity(user, name, start, end, contacts, description, location);
    }

    public List<Activity> activitiesOnDate(User user, Date d, Date e) throws InvalidFormatException {
        return activityService.activitiesOnDate(user, d, e);
    }
}
