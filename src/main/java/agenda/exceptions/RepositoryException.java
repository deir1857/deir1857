package agenda.exceptions;

public class RepositoryException extends Exception{
	private static final long serialVersionUID = -6262759468431626763L;

	public RepositoryException(String msg)
	{
		super(msg);
	}

	public RepositoryException(String msg, String reason)
	{
		super(msg, new Throwable(reason));
	}	
	
}
