package agenda.gui.console;

import agenda.controller.Controller;
import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by elisei on 12.03.2018.
 */
public class UI {

    private final Controller ctrl;

    public UI(Controller ctrl){
        this.ctrl = ctrl;
    }

    private void afisActivitate(BufferedReader in, User user) {
        try {
            System.out.printf("Afisare Activitate: \n");
            System.out.printf("Data(format: mm/dd/yyyy): ");
            String dateS = in.readLine();
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]),
                    0, 0, 0);
            Date d = c.getTime();
            c.add(Calendar.DATE, 1);
            Date e = c.getTime();

            System.out.println("Activitatile din ziua " + d.toString() + ": ");

            List<Activity> act = ctrl.activitiesOnDate(user, d, e);
            for (Activity a : act) {
                System.out.printf("%s - %s: %s - %s with: ", a.getStart()
                        .toString(), a.getEnd().toString(), a
                        .getDescription(), a.getName());
                for (Contact con : a.getContacts())
                    System.out.printf("%s, ", con.getName());
                System.out.println();
            }
        } catch (IOException e) {
            System.out.printf("Eroare de citire: %s\n" + e.getMessage());
        }
        catch (InvalidFormatException e){
            if (e.getCause() != null)
                System.out.printf("Eroare: %s - %s\n", e.getMessage(), e
                        .getCause().getMessage());
            else
                System.out.printf("Eroare: %s\n", e.getMessage());
        }
    }

    private void adaugActivitate(BufferedReader in, User user) {
        try {
            System.out.printf("Adauga Activitate: \n");
            System.out.printf("Nume: ");
            String name = in.readLine();
            System.out.printf("Descriere: ");
            String description = in.readLine();
            System.out.printf("Start Date(format: mm/dd/yyyy): ");
            String dateS = in.readLine();
            System.out.printf("Start Time(hh:mm): ");
            String timeS = in.readLine();
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]),
                    Integer.parseInt(timeS.split(":")[0]),
                    Integer.parseInt(timeS.split(":")[1]), 0);
            Date start = c.getTime();

            System.out.printf("End Date(format: mm/dd/yyyy): ");
            String dateE = in.readLine();
            System.out.printf("End Time(hh:mm): ");
            String timeE = in.readLine();

            c.set(Integer.parseInt(dateE.split("/")[2]),
                    Integer.parseInt(dateE.split("/")[0]) - 1,
                    Integer.parseInt(dateE.split("/")[1]),
                    Integer.parseInt(timeE.split(":")[0]),
                    Integer.parseInt(timeE.split(":")[1]), 0);
            Date end = c.getTime();

            System.out.printf("Location: ");
            String location = in.readLine();
            ctrl.addActivity(user, name, start, end, new LinkedList<Contact>(), description, location);

            System.out.printf("S-a adugat cu succes\n");
        } catch (IOException e) {
            System.out.printf("Eroare de citire: %s\n" , e.getMessage());
        }
        catch (InvalidFormatException|RepositoryException e){
            if (e.getCause() != null)
                System.out.printf("Eroare: %s - %s\n", e.getMessage(), e
                        .getCause().getMessage());
            else
                System.out.printf("Eroare: %s\n", e.getMessage());
        }

    }

    private void adaugContact(BufferedReader in, User user) {

        try {
            System.out.printf("Adauga Contact: \n");
            System.out.printf("Nume: ");
            String name = in.readLine();
            System.out.printf("Adresa: ");
            String address = in.readLine();
            System.out.printf("Numar de telefon: ");
            String telefon = in.readLine();
            System.out.printf("Email: ");
            String email = in.readLine();

            ctrl.addContact(name, address, telefon, email, user);

            System.out.printf("S-a adugat cu succes\n");
        } catch (IOException e) {
            System.out.printf("Eroare de citire: %s\n",  e.getMessage());
        } catch (InvalidFormatException|RepositoryException e) {
            if (e.getCause() != null)
                System.out.printf("Eroare: %s - %s\n", e.getMessage(), e
                        .getCause().getMessage());
            else
                System.out.printf("Eroare: %s\n", e.getMessage());
        }

    }

    private static void printMenu() {
        System.out.printf("Please choose option:\n");
        System.out.printf("1. Adauga contact\n");
        System.out.printf("2. Adauga activitate\n");
        System.out.printf("3. Afisare activitati din data de...\n");
        System.out.printf("4. Exit\n");
        System.out.printf("Alege: ");
    }


    public void run(){
        BufferedReader in;
        User user = null;
        in = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (user == null) {
                System.out.printf("Enter username: ");
                String u = in.readLine();
                System.out.printf("Enter password: ");
                String p = in.readLine();
                user = new User("", u, p);
                if(!ctrl.canLogIN(u, p)){
                    user = null;
                    System.out.println("Invalid user and password inserted!");
                }
            }

            int chosen = 0;
            while (chosen != 4) {
                printMenu();

                String line = in.readLine().trim();
                try {
                    chosen = Integer.parseInt(line);
                }
                catch (Exception e){
                    System.out.println("Invalid option");
                    continue;
                }

                try {
                    switch (chosen) {
                        case 1:
                            adaugContact(in, user);
                            break;
                        case 2:
                            adaugActivitate(in, user);
                            break;
                        case 3:
                            afisActivitate(in, user);
                            break;
                        case 4:
                            break;
                        default:
                            System.out.println("Invalid option");
                            break;
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                System.out.println();
            }
        }
        catch (Exception e){
            System.out.println("Failed to process input..");
            e.printStackTrace();
        }
    }
}
