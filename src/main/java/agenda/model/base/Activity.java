package agenda.model.base;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;

public class Activity {
	private String name;
	private Date start;
	private Date end;
	private List<Contact> contacts;
	private String description;
	private User user;
	private String location;

	public Activity(String name, Date start, Date end, List<Contact> contacts,
					String description, User user, String location) {
		this.name = name;
		this.description = description;
		if (contacts == null)
			this.contacts = new LinkedList<Contact>();
		else
			this.contacts = new LinkedList<Contact>(contacts);

		this.location = location;
		this.start = new Date();
		this.start.setTime(start.getTime());
		this.end = new Date();
		this.end.setTime(end.getTime());
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {return  location;}

	public void setLocation(String value){
		this.location = location;
	}
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Activity))
			return false;
		Activity act = (Activity) obj;
		if (act.description.equals(description) && start.equals(act.start)
				&& end.equals(act.end) && name.equals(act.name))
			return true;
		return false;
	}

	public boolean intersect(Activity act) {
		if (start.compareTo(act.end) < 0
				&& act.start.compareTo(end) < 0)
			return true;
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("#");
		sb.append(start.getTime());
		sb.append("#");
		sb.append(end.getTime());
		sb.append("#");
		sb.append(user.getUsername());
		sb.append("#");
		sb.append(description);
		sb.append("#");
		sb.append(location);
		sb.append("#");
		for (Contact c : contacts) {
			sb.append("#");
			sb.append(c.getName());
		}
		return sb.toString();
	}

	public static Activity fromString(String line, IRepositoryContact repcon, IRepositoryUser repuser) {
		try {
			String[] str = line.split("#");
			String name = str[0];
			Date start = new Date(Long.parseLong(str[1]));
			Date end = new Date(Long.parseLong(str[2]));
			String location = str[5];
			String description = str[4];
			User user = repuser.getByUsername(str[3]);
			if(user == null)
				return null;

			List<Contact> conts = new LinkedList<Contact>();
			for (int i = 6; i < str.length; i++) {
				String contName = str[i].trim();
				if(contName.isEmpty()) continue;

				Contact contact = repcon.getByName(str[i]);
				if(contact == null)
					return null;

				conts.add(contact);
			}
			return new Activity(name, start, end, conts, description, user, location);
		} catch (Exception e) {
			return null;
		}
	}
}
