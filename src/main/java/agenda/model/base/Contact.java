package agenda.model.base;

import agenda.exceptions.InvalidFormatException;
import agenda.repository.interfaces.IRepositoryUser;

public class Contact {
	private String name;
	private String address;
	private String telefon;
	private String email;
	private User user;
	
	public Contact(){
		name = "";
		address = "";
		telefon = "";
		email = "";
		user = null;
	}
	
	public Contact(String name, String address, String telefon, String email, User user){
		this.name = name;
		this.address = address;
		this.telefon = telefon;
		this.email = email;
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws InvalidFormatException {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		this.address = address;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		this.telefon = telefon;
	}


	public String getEmail(){return email;}
	public void setEmail(String email){this.email = email;}

	public User getUser(){return  user;}
	public void setUser(User user) { this.user = user;}


	public static Contact fromString(String str, String delim,  IRepositoryUser repuser) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		String name, address, username, telefon, email;

		if (s.length != 5) throw new InvalidFormatException("Cannot convert", "Invalid data");

		name = s[0];
		address = s[1];
		telefon = s[2];
		email = s[3];
		username = s[4];
		User user = repuser.getByUsername(username);
		if(user == null)
			throw  new InvalidFormatException("Cannot find user", "Invalid username");

		return new Contact(name, address, telefon, email, user);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("#");
		sb.append(address);
		sb.append("#");
		sb.append(telefon);
		sb.append("#");
		sb.append(email);
		sb.append("#");
		sb.append(user != null ? user.getUsername() : "");
		return sb.toString();
	}
	

		
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (name.equals(o.name) && address.equals(o.address) &&
				telefon.equals(o.telefon) && email.equals(o.email) && user.getUsername().equals(o.user.getUsername()))
			return true;
		return false;
	}
	
}
