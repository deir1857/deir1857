package agenda.model.validator;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;

import java.util.Date;

/**
 * Created by elisei on 12.03.2018.
 */
public class ActivityValidator implements Validator<Activity> {
    @Override
    public void validate(Activity activity) throws InvalidFormatException {
        if (!validName(activity.getName())) throw new InvalidFormatException("Cannot convert", "Invalid name");
        if (!validDescription(activity.getDescription())) throw new InvalidFormatException("Cannot convert", "Invalid description");
        if (!validName(activity.getLocation())) throw new InvalidFormatException("Cannot convert", "Invalid location");
        if(!validDate(activity.getStart())) throw new InvalidFormatException("Cannot convert", "Invalid start date");
        if(!validInterval(activity.getStart(), activity.getEnd())) throw new InvalidFormatException("Cannot convert", "Invalid duration");

    }

    private boolean validInterval(Date start, Date end) {
        if(end == null || start.compareTo(end) > 0 )
            return false;
        return true;
    }

    private boolean validDate(Date start) {
        if(start == null || start.compareTo(new Date()) < 0)
            return false;
        return true;
    }


    private boolean validName(String name) {
        if(name == null || name.isEmpty()) return false;
        return name.length() >= 1 && name.length() <= 20;
    }

    private boolean validDescription(String description){
        if(description == null || description.isEmpty()) return false;
        return description.length() >= 1 && description.length() <= 50;
    }


}
