package agenda.model.validator;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;

/**
 * Created by elisei on 12.03.2018.
 */
public class ContactValidator implements Validator<Contact> {
    @Override
    public void validate(Contact contact) throws InvalidFormatException {
        if (!validName(contact.getName())) throw new InvalidFormatException("Invalid format", "Invalid name");
        if (!validTelefon(contact.getTelefon())) throw new InvalidFormatException("Invalid format", "Invalid phone number");
        if (!validAddress(contact.getAddress())) throw new InvalidFormatException("Invalid format", "Invalid address");
        if(!validEmail(contact.getEmail())) throw new InvalidFormatException("Invalid format", "Invalid email");
    }

    private static boolean validName(String str)
    {
        if(str == null || str.isEmpty()) return false;
        if(str.length() > 50) return false;

        return str.matches("^[A-Z][A-Za-z]*([ -][A-Z][A-Za-z]*)? [A-Z][A-Za-z]*([ -][A-Z][A-Za-z]*)?$");
    }

    private static boolean validAddress(String str)
    {
        if(str == null || str.isEmpty()) return false;
        if(str.length() > 100) return false;

        return str.matches("^Strada ([a-zA-Z]+([ -][a-zA-Z]+)*) nr\\. \\d+, Oras ([a-zA-Z]+([ -][a-zA-Z]+)*), Judet ([a-zA-Z]+([ -][a-zA-Z]+)*), Tara ([a-zA-Z]+([ -][a-zA-Z]+)*)$");
    }

    private static boolean validTelefon(String str)
    {
        if(str == null  || str.isEmpty()) return false;
        if(str.length() > 12) return false;

        return str.matches("^\\+40\\d{9}$");
    }

    private static boolean validEmail(String str){
        if(str == null || str.isEmpty()) return false;
        return str.length() <= 40;
    }

}
