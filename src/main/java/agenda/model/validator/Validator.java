package agenda.model.validator;

import agenda.exceptions.InvalidFormatException;

/**
 * Created by elisei on 12.03.2018.
 */
public interface Validator<E> {
    void validate(E e) throws InvalidFormatException;
}
