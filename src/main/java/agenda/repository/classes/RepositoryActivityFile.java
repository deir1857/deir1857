package agenda.repository.classes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;

import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.User;
import agenda.repository.interfaces.IRepositoryActivity;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;

public class RepositoryActivityFile implements IRepositoryActivity {

	private static final String DEFAULT_FILENAME= "bin/files/activities.txt";
	private String filename;
	private List<Activity> activities;

	public RepositoryActivityFile(IRepositoryContact repcon, IRepositoryUser repuser, String filename) throws Exception {
		this.filename = filename;
		activities = new LinkedList<Activity>();
		//DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
			String line;
			int i = 0;
			while (( line = br.readLine())!= null)
			{
				if(line.isEmpty())
					continue;
				Activity act = Activity.fromString(line, repcon, repuser);
				if (act == null)
					throw new Exception("Error in file at line "+i, new Throwable("Invalid Activity"));
				activities.add(act);
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if (br!=null) br.close();
		}
	}

	public RepositoryActivityFile(IRepositoryContact repcon, IRepositoryUser repuser) throws Exception
	{
		this(repcon, repuser, DEFAULT_FILENAME);
	}
	
	@Override
	public List<Activity> getActivities() {
		return new LinkedList<Activity>(activities);
	}

	@Override
	public void addActivity(Activity activity) throws RepositoryException {
		int  i = 0;
		boolean conflicts = false;
		
		while( i < activities.size() )
		{
			if ( activities.get(i).getStart().compareTo(activity.getEnd()) < 0 &&
					activity.getStart().compareTo(activities.get(i).getEnd()) < 0 )
				conflicts = true;
			i++;
		}
		if ( conflicts )
			throw  new RepositoryException("Cannot add activity", "Conflict");

		activities.add(activity);
		saveActivities();
	}

	@Override
	public void removeActivity(Activity activity) throws RepositoryException  {
		int index = activities.indexOf(activity);
		if (index < 0)
			throw new RepositoryException("Cannot remove activity", "Doesn't exist");
		activities.remove(index);
	}

	@Override
	public void saveActivities() throws RepositoryException  {
		PrintWriter pw = null;
		try{
			pw = new PrintWriter(new FileOutputStream(filename));
			for(Activity a : activities)
				pw.println(a.toString());
		}catch (Exception e)
		{
			e.printStackTrace();
			throw new RepositoryException("Cannot save data to file");
		}
		finally{
			if (pw!=null) pw.close();
		}
	}

	@Override
	public int count() {
		return activities.size();
	}
	
	@Override
	public List<Activity> activitiesByName(String name) {
		List<Activity> result1 = new LinkedList<Activity>();
		for (Activity a : activities)
			if (a.getName().equals(name) == false) result1.add(a);
		List<Activity> result = new LinkedList<Activity>();
		while (result1.size() >= 0 )
		{
			Activity ac = result1.get(0);
			int index = 0;
			for (int i = 1; i<result1.size(); i++)
				if (ac.getStart().compareTo(result1.get(i).getStart())<0) 
				{
					index = i;
					ac = result1.get(i);
				}
			
			result.add(ac);
			result1.remove(index);
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Activity> activitiesOnDate(User user, Date d, Date e) {
		List<Activity> result1 = new LinkedList<Activity>();

		for (Activity a : activities)
			if (a.getUser().getUsername().equals(user.getUsername()))
				if ( (a.getStart().compareTo(d)>= 0 && a.getStart().compareTo(e) <= 0) ||
						(a.getEnd().compareTo(d) >= 0 && a.getEnd().compareTo(e)<= 0))
					result1.add(a);

		Collections.sort(result1, (a1, a2 ) ->{
			return a1.getStart().compareTo(a2.getStart());
		});
		return result1;
	}
}
