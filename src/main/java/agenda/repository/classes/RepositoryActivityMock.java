package agenda.repository.classes;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.User;
import agenda.repository.interfaces.IRepositoryActivity;

public class RepositoryActivityMock implements IRepositoryActivity {

	private List<Activity> activities;
	
	public RepositoryActivityMock()
	{
		activities = new LinkedList<Activity>();
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try {
//			Activity act = new Activity(df.parse("03/20/2013 12:00"), 
//					df.parse("03/20/2013 14:00"),
//					null,
//					"Meal break",
//					"Memo");
//			activities.add(act);
//			act = new Activity(df.parse("03/21/2013 12:00"), 
//					df.parse("03/21/2013 14:00"),
//					null,
//					"Meal break",
//					"Memo");
//			activities.add(act);
//			act = new Activity(df.parse("03/22/2013 12:00"), 
//					df.parse("03/22/2013 14:00"),
//					null,
//					"Meal break",
//					"Memo");
//			activities.add(act);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
	}
	
	@Override
	public List<Activity> getActivities() {
		return new LinkedList<Activity>(activities);
	}

	@Override
	public void addActivity(Activity activity) throws RepositoryException {
		int  i = 0;
		boolean conflicts = false;
		
		while( i < activities.size() )
		{
			if ( activities.get(i).getStart().compareTo(activity.getEnd()) < 0 &&
					activity.getStart().compareTo(activities.get(i).getEnd()) < 0 )
				conflicts = true;
			i++;
		}
		if ( conflicts )
			throw new RepositoryException("Cannot add activity", "Conflict");
		activities.add(activity);
//		for (int i = 0; i< activities.size(); i++)
//		{
//			if (activity.intersect(activities.get(i))) return false;
//		}	
//		int index = activities.indexOf(activity);
//		//if (index >= 0 ) return false;
//		activities.add(activity);
//		return true;
	}

	@Override
	public void  removeActivity(Activity activity) throws RepositoryException {
		int index = activities.indexOf(activity);
		if (index < 0)
			throw new RepositoryException("Invalid activity!");
		activities.remove(index);
	}

	@Override
	public void saveActivities() {

	}

	@Override
	public int count() {
		return activities.size();
	}

	@Override
	public List<Activity> activitiesByName(String name) {
		List<Activity> result = new LinkedList<Activity>();
		for (Activity a : activities)
			if (a.getName().equals(name)) result.add(a);
		return result;
	}

	@Override
	public List<Activity> activitiesOnDate(User user, Date d, Date e) {
		List<Activity> result = new LinkedList<Activity>();
		for (Activity a : activities)
			if (a.getName().equals(user.getUsername()))
				if (a.getStart().compareTo(d) <= 0 && d.compareTo(a.getEnd()) <= 0 ) result.add(a);
		return result;
	}

}
