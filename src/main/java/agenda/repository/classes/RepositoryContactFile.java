package agenda.repository.classes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Contact;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;

public class RepositoryContactFile implements IRepositoryContact {

	private static final String DEFAULT_FILENAME = "bin/files/contacts.txt";
	private String filename;
	private List<Contact> contacts;

	public RepositoryContactFile(IRepositoryUser repUser, String filename) throws  Exception{
		contacts = new LinkedList<Contact>();
		this.filename = filename;
		BufferedReader br = null;
//		String currentDir = new File(".").getAbsolutePath();
//		System.out.println(currentDir);
		try {
			br = new BufferedReader(new InputStreamReader(
					new FileInputStream(filename)));
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				Contact c = null;
				try {
					c = Contact.fromString(line, "#", repUser);
				} catch (InvalidFormatException e) {
					throw new Exception("Error in file at line " + i,
							new Throwable(e.getCause().getMessage()));
				}
				contacts.add(c);
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally
		{
			if (br != null) br.close();
		}
	}

	public RepositoryContactFile(IRepositoryUser repUser) throws Exception {
		this(repUser, DEFAULT_FILENAME);
	}

	@Override
	public List<Contact> getContacts() {
		return new LinkedList<Contact>(contacts);
	}

	@Override
	public void addContact(Contact contact) throws RepositoryException {

		contacts.add(contact);
		saveContracts();
	}

	@Override
	public void removeContact(Contact contact) throws RepositoryException {
		int index = contacts.indexOf(contact);
		if (index < 0)
			throw  new RepositoryException("Invalid contact");
		contacts.remove(index);
	}

	@Override
	public void saveContracts() throws RepositoryException {
		PrintWriter pw = null;
		try{
			pw = new PrintWriter(new FileOutputStream(filename));
			for(Contact c : contacts)
				pw.println(c.toString());
		}catch (Exception e)
		{
			throw new RepositoryException("Failed to save data to file", e.getMessage());
		}
		finally{
			if (pw!=null) pw.close();
		}
	}

	@Override
	public int count() {
		return contacts.size();
	}

	@Override
	public Contact getByName(String string) {
		for (Contact c : contacts)
			if (c.getName().equals(string))
				return c;
		return null;
	}

}
