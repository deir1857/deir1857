package agenda.repository.classes;

import java.util.LinkedList;
import java.util.List;

import agenda.exceptions.RepositoryException;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.exceptions.InvalidFormatException;

public class RepositoryContactMock implements IRepositoryContact {

private List<Contact> contacts;
	
	public RepositoryContactMock() {
		contacts = new LinkedList<Contact>();
		User user = new User("asd", "asd", "asd");
		Contact c = new Contact("Name1", "address1", "+4071122334455", "elisie", user);
		contacts.add(c);
		c = new Contact("Name2", "address 2", "+4071122334466", "email", user);
		contacts.add(c);
		c = new Contact("Name3", "address 3", "+4071122338866","email", user);
		contacts.add(c);
	}

	@Override
	public List<Contact> getContacts() {
		return new LinkedList<Contact>(contacts);
	}

	@Override
	public void addContact(Contact contact) throws RepositoryException {
		contacts.add(contact);
	}

	@Override
	public void removeContact(Contact contact) throws RepositoryException {
		int index = contacts.indexOf(contact);
		if (index < 0)
			throw  new RepositoryException("Invalid contact");
		contacts.remove(index);
	}

	@Override
	public void saveContracts() throws RepositoryException {

	}

	@Override
	public int count() {
		return contacts.size();
	}

	@Override
	public Contact getByName(String string) {
		for(Contact c : contacts)
			if (c.getName().equals(string)) return c;
		return null;
	}

}
