package agenda.repository.interfaces;

import java.util.Date;
import java.util.List;

import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.User;

public interface IRepositoryActivity {

	List<Activity> getActivities();
	void addActivity(Activity activity) throws RepositoryException;
	void removeActivity(Activity activity) throws RepositoryException;
	void saveActivities() throws RepositoryException;
	int count();
	List<Activity> activitiesByName(String name);
	List<Activity> activitiesOnDate(User user, Date d, Date e);
	
}
