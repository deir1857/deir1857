package agenda.repository.interfaces;

import java.util.List;

import agenda.exceptions.RepositoryException;
import agenda.model.base.Contact;

public interface IRepositoryContact {

	List<Contact> getContacts();
	void addContact(Contact contact) throws RepositoryException;
	void removeContact(Contact contact) throws RepositoryException;
	void  saveContracts() throws RepositoryException;
	int count();
	Contact getByName(String string);
}
