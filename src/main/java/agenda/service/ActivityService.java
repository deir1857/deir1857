package agenda.service;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.validator.ActivityValidator;
import agenda.repository.interfaces.IRepositoryActivity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by elisei on 02.04.2018.
 */
public class ActivityService {

    private final ActivityValidator activityValidator;
    private final IRepositoryActivity repositoryActivity;

    public ActivityService(IRepositoryActivity repositoryActivity , ActivityValidator activityValidator){
        this.activityValidator = activityValidator;
        this.repositoryActivity = repositoryActivity;
    }

    public void addActivity(User user, String name, Date start, Date end, LinkedList<Contact> contacts, String description, String location) throws InvalidFormatException, RepositoryException{
        Activity activity = new Activity(name, start, end, contacts, description, user, location);
        activityValidator.validate(activity);
        repositoryActivity.addActivity(activity);
    }

    public List<Activity> activitiesOnDate(User user, Date d, Date e) throws InvalidFormatException {
        if(d.compareTo(e) > 0)
            throw new InvalidFormatException("Invalid dates: End date is lower than start date");
        return repositoryActivity.activitiesOnDate(user, d, e);
    }
}
