package agenda.service;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.validator.ContactValidator;
import agenda.repository.interfaces.IRepositoryContact;

import java.util.List;

/**
 * Created by elisei on 02.04.2018.
 */
public class ContactService {

    private final ContactValidator contactValidator;
    private final IRepositoryContact repositoryContact;

    public ContactService(IRepositoryContact contactRepository, ContactValidator contactValidator){
        this.contactValidator = contactValidator;
        this.repositoryContact = contactRepository;

    }

    public void addContact(String name, String address, String telefon, String email, User user) throws RepositoryException, InvalidFormatException{
        Contact contact = new Contact(name, address, telefon, email, user);
        contactValidator.validate(contact);
        repositoryContact.addContact(contact);
    }

    public List<Contact> getContacts() {
        return repositoryContact.getContacts();
    }
}
