package agenda.service;

import agenda.model.base.User;
import agenda.repository.interfaces.IRepositoryUser;

/**
 * Created by elisei on 02.04.2018.
 */
public class UserService {

    public final IRepositoryUser repositoryUser;

    public UserService(IRepositoryUser repositoryUser){
        this.repositoryUser = repositoryUser;
    }


    public boolean canLogIn(String username, String password){
        User foundUser = repositoryUser.getByUsername(username);
        return !(foundUser == null || !foundUser.isPassword(password));
    }
}
