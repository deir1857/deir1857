package agenda.springboot;

import agenda.model.validator.ContactValidator;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import agenda.service.ContactService;
import agenda.service.UserService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * Created by elisei on 17.05.2018.
 */
@EnableAutoConfiguration
@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public IRepositoryUser repositoryUser() throws Exception {
        return new RepositoryUserFile();
    }

    @Bean
    public UserService userService(IRepositoryUser repositoryUser){
        return new UserService(repositoryUser);
    }

    @Bean
    public IRepositoryContact contactRepository(IRepositoryUser repositoryUser)
            throws Exception {
        return new RepositoryContactFile(repositoryUser);
    }

    @Bean
    public ContactValidator contactValidator(){
        return new ContactValidator();
    }

    @Bean
    public ContactService contactService(IRepositoryContact contactRepository,
                                         ContactValidator contactValidator){
        return new ContactService(contactRepository, contactValidator);
    }
}
