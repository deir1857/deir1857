package agenda.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by elisei on 17.05.2018.
 */
@SpringBootApplication
public class StartAppSpringBoot {
    public static void main(String[] args){
        SpringApplication.run(StartAppSpringBoot.class, args);
    }
}
