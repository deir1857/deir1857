package agenda.springboot.config;

import agenda.springboot.model.Response;
import agenda.springboot.model.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by elisei on 17.05.2018.
 */
@Component
public class AuthFailedHandler extends SimpleUrlAuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        ArrayList<String> errors = new ArrayList<String>();
        errors.add("Invalid username or password!");
        Response response1  = new Response(Status.STATUS_FAILED, errors);
        ObjectMapper objectMapper = new ObjectMapper();
        response.setStatus(HttpServletResponse.SC_OK);
        objectMapper.writeValue(response.getWriter(), response1);
        response.getWriter().flush();
    }
}
