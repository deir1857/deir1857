package agenda.springboot.controller;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.User;
import agenda.service.ContactService;
import agenda.service.UserService;
import agenda.springboot.model.ContactModel;
import agenda.springboot.model.Response;
import agenda.springboot.model.Status;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;

/**
 * Created by elisei on 17.05.2018.
 */
@CrossOrigin
@RestController
public class ContactRESTController {

    @Autowired
    private UserService userService;

    @Autowired
    private ContactService contactService;


    @PostMapping(value = "/contact")
    public Response addContact(@RequestBody ContactModel contactModel, Principal user) throws InvalidFormatException, RepositoryException {
        ArrayList<String> errors = new ArrayList<>();
        if(user == null){
            errors.add("You are not logged in!");
            return new Response(Status.STATUS_NOT_LOGGED_IN, errors);
        }
        try{
            contactService.addContact(
                    contactModel.getName(),
                    contactModel.getAddress(),
                    contactModel.getTelefon(),
                    contactModel.getEmail(),
                    userService.repositoryUser.getByUsername(user.getName())
            );
        }
        catch (InvalidFormatException|RepositoryException e){
            errors.add(e.getMessage() + " " + e.getCause().getMessage());
            return new Response(Status.STATUS_FAILED, errors);
        }
        return new Response(Status.STATUS_OK, errors);

    }

    @GetMapping(value = "/contact")
    public Response getContacts(Principal user){
        ArrayList<String> errors = new ArrayList<>();
        if(user == null){
            errors.add("You are not logged in!");
            return new Response(Status.STATUS_NOT_LOGGED_IN, errors);
        }
        return new Response(Status.STATUS_OK, errors, new Pair<>("contacts", contactService.getContacts()));
    }
}
