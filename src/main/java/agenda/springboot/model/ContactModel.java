package agenda.springboot.model;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import agenda.model.base.User;

/**
 * Created by elisei on 17.05.2018.
 */
public class ContactModel {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private String name;
    private String address;
    private String telefon;
    private String email;
    private User user;

    public ContactModel(){}

    public ContactModel(String name, String address, String telefon, String email, User user) {
        this.name = name;
        this.address = address;
        this.telefon = telefon;
        this.email = email;
        this.user = user;
    }

    public Contact toContact(){
        return new Contact(name, address, telefon, email, user);
    }
}
