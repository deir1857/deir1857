package agenda.springboot.model;

/**
 * Created by elisei on 17.05.2018.
 */
public enum Status {
    STATUS_NOT_LOGGED_IN,
    STATUS_PERMISSION_DENIED,
    STATUS_FAILED,
    STATUS_OK
}
