package agenda.startApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.controller.Controller;
import agenda.exceptions.InvalidFormatException;

import agenda.gui.console.UI;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.validator.ActivityValidator;
import agenda.model.validator.ContactValidator;
import agenda.model.validator.UserValidator;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.IRepositoryActivity;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import agenda.service.ActivityService;
import agenda.service.ContactService;
import agenda.service.UserService;

//functionalitati
//i.	 adaugarea de contacte (nume, adresa, numar de telefon, adresa email);
//ii.	 programarea unor activitati (denumire, descriere, data, locul, ora inceput, durata, contacte).
//iii.	 generarea unui raport cu activitatile pe care le are utilizatorul (nume, user, parola) la o anumita data, ordonate dupa ora de inceput.

public class MainClass {

	public static void main(String[] args) {
	    IRepositoryUser userRep;
	    IRepositoryContact contactRep;
	    IRepositoryActivity activityRep;
	    Controller ctrl;
	    UI ui;
		UserService userService;
		ActivityService activityService;
		ContactService contactService;

		try {
			userRep = new RepositoryUserFile();
			contactRep = new RepositoryContactFile(userRep);
			activityRep = new RepositoryActivityFile(contactRep, userRep);

			// List<Activity> act =
			// activityRep.activitiesByName(user.getName());
			// for(Activity a : act)
			// System.out.println(a.toString());

		} catch (Exception e) {
            System.out.println("Cannot initialize repositories");
            e.printStackTrace();
            return;
        }

		userService = new UserService(userRep);
		activityService = new ActivityService(activityRep, new ActivityValidator());
		contactService = new ContactService(contactRep, new ContactValidator());
        ctrl = new Controller(contactService, activityService, userService);
        ui = new UI(ctrl);
        ui.run();
		System.out.println("Program over and out\n");
	}
}
