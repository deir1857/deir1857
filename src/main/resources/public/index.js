/**
 * Created by elisei on 17.05.2018.
 */
var addContactForm, addContactButton;
var nameInput, addressInput, telephoneInput, emailInput;

function addContact(){
    var name = nameInput.val();
    var telephone = telephoneInput.val();
    var address = addressInput.val();
    var email = emailInput.val();

    $.ajax({
        type: "POST",
        url: "/contact",
        data: JSON.stringify({name:name, address:address, telefon: telephone, email: email}),
        dataType: "text",
        cache: false,
        timeout: 6000,
        contentType:"application/json; charset=utf-8",
        success:function(data){
            data = JSON.parse(data);
            if(data.data.status == "STATUS_OK"){
                alert("Added succeeded!");
            }
            else{
                alert(data.errors.join("\n"));
            }
        },
        error: function(){
            console.log("Error");
        }
    });
}

function init(){
    addContactForm = $("#addContactForm");
    addContactButton = $("#addContactButton").click(addContact);

    nameInput = $("#name");
    addressInput = $("#address");
    telephoneInput = $("#telephone");
    emailInput = $("#email");
}

$(init);
