/**
 * Created by elisei on 17.05.2018.
 */
var loginForm, loginButton;
var usernameInput, passwordInput;

function login(){
    var username = usernameInput.text();
    var password = passwordInput.text();

    var data = new FormData(loginForm[0]);

    data.append("username", $('#username').val());
    data.append("password", $('#password').val());

    $.ajax({
        type: "POST",
        enctype: "multipart/form-data",
        url: "/login",
        data: data,
        processData:false,
        contentType:false,
        cache: false,
        timeout: 6000,
        success:function(data){
            data = JSON.parse(data);
            if(data.data.status == "STATUS_OK"){
                alert("Login succeeded!");
                //window.location = "/index.html";
            }
            else{
                alert(data.errors.join("\n"));
            }
        },
        error: function(){
            console.log("Error");
        }
    });
}

function init(){
    loginForm = $("#loginForm");
    loginButton = $("#loginButton").click(login);
    usernameInput = $("#username");
    passwordInput = $("#password");
}

$(init);
