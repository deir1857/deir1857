package agenda;

import java.io.File;
import java.io.IOException;

/**
 * Created by elisei on 07.05.2018.
 */
public class Utils {



    public static void emptyFile(String fileName) throws IOException {
        File file = new File(fileName);
        if(file.exists())
            file.delete();
        file.createNewFile();
    }

    public static void emptyRepository(){
        try{
            emptyFile("bin/files/testActivity.txt");
            emptyFile("bin/files/testContact.txt");
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

}
