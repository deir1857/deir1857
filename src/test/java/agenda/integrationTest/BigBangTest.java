package agenda.integrationTest;

import agenda.controller.Controller;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.validator.ActivityValidator;
import agenda.model.validator.ContactValidator;
import agenda.repository.RepositoryActivityTest;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.IRepositoryActivity;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import agenda.service.*;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static agenda.Utils.emptyRepository;

/**
 * Created by elisei on 07.05.2018.
 */


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BigBangTest {

    private User user;


    @Test
    public void test01UnitA() throws Exception{
        ContactServiceTest test = new ContactServiceTest();
        test.setUp();
        test.addContactTC1_ECP();
        test.setUp();
        test.addContactTC3_ECP();
        test.setUp();
        test.addContactTC4_ECP();
        test.setUp();
        test.addContactTC8_ECP();
        test.setUp();
        test.addContactTC10_ECP();
        test.setUp();
        test.addContactTC11_ECP();

        test.setUp();
        test.addContactTC3_BVA();
        test.setUp();
        test.addContactTC4_BVA();
        test.setUp();
        test.addContactTC5_BVA();
        test.setUp();
        test.addContactTC15_BVA();
    }

    @Test
    public void test02UnitB() throws Exception{
        RepositoryActivityTest test = new RepositoryActivityTest();
        test.setUp();
        test.addActivity_TC01();;
        test.setUp();
        test.addActivity_TC02();
        test.setUp();
        test.addActivity_TC03();
        test.setUp();
        test.addActivity_TC04();
        test.setUp();
        test.addActivity_TC05();
    }

    @Test
    public void test03UnitC() throws Exception{
        ActivityServiceTest test = new ActivityServiceTest();
        test.setUp();
        test.generateReportValidCase();
        test.setUp();
        test.generateReportInvalidCase();
    }

    public Controller setUp() throws Exception{
        emptyRepository();

        IRepositoryUser  repositoryUser = new RepositoryUserFile("bin/files/testUser.txt");
        IRepositoryContact repositoryContact = new RepositoryContactFile(repositoryUser, "bin/files/testContact.txt");
        IRepositoryActivity repositoryActivity = new RepositoryActivityFile(repositoryContact, repositoryUser, "bin/files/testActivity.txt");
        user = repositoryUser.getByUsername("username1");
        return new Controller(
                new ContactService(repositoryContact, new ContactValidator()),
                new ActivityService(repositoryActivity, new ActivityValidator()),
                new UserService(repositoryUser)
            );
    }

    @Test
    public void test04Integration() throws  Exception{
        List<Activity> result;
        Controller ctrl = setUp();
        assert ctrl.canLogIN("username1", "pass1");

        try {
            ctrl.addContact("Elisei Vasile",
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "Elisei",
                    user
            );
            assert true;
        }
        catch (Exception c){
            assert false;
        }

        try{
            ctrl.addActivity(
                    user,
                    "Name", new Date(2017, 06, 1),
                    new Date(2017, 06, 2),
                    new LinkedList<>(),
                    "Description",
                    "Location"
            );
            assert true;
        }
        catch (Exception e){
            assert false;
        }


        try{
            result = ctrl.activitiesOnDate(user,
                    new Date(2017, 06, 1),
                    new Date(2017, 06, 2)
            );
            assert result.size() == 1;

            Activity found = result.get(0);
            assert found.getName().equals("Name");
            assert found.getDescription().equals("Description");
            assert found.getLocation().equals("Location");
            assert found.getStart().equals(new Date(2017, 06, 1));
            assert found.getEnd().equals(new Date(2017, 06, 2));
            assert found.getContacts().size() == 0;

        }
        catch (Exception c){
            assert false;
        }
    }

}
