package agenda.repository;

import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.User;
import agenda.model.validator.ContactValidator;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.IRepositoryActivity;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import agenda.service.ContactService;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by elisei on 23.04.2018.
 */
public class RepositoryActivityTest {


    private IRepositoryActivity repositoryActivity;
    private IRepositoryContact repositoryContact ;
    private IRepositoryUser repositoryUser;
    private User user;


    public void emptyRepository(){
        try{

            File file = new File("bin/files/testActivity.txt");
            if(file.exists())
                file.delete();
            file.createNewFile();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Before
    public void setUp() throws Exception{
        emptyRepository();

        repositoryUser = new RepositoryUserFile("bin/files/testUser.txt");
        repositoryContact = new RepositoryContactFile(repositoryUser, "bin/files/testContact.txt");
        repositoryActivity = new RepositoryActivityFile(repositoryContact, repositoryUser, "bin/files/testActivity.txt");
        user = repositoryUser.getByUsername("username1");

        Activity activity = new Activity(
                "Name", new Date(2017, 06, 03),
                new Date(2017, 06, 04),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);

        } catch (RepositoryException e) {
            assert false;
        }
    }


    @Test
    public void addActivity_TC01(){

        Activity activity = new Activity(
                "Name", new Date(2017, 06, 01),
                new Date(2017, 06, 04),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);
            assert false;
        } catch (RepositoryException e) {
            assert true;
        }
    }

    @Test
    public void addActivity_TC02(){
        Activity activity = new Activity(
                "Name", new Date(2017, 06, 1),
                new Date(2017, 06, 2),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);
            assert true;
        } catch (RepositoryException e) {
            assert false;
        }
    }


    @Test
    public void addActivity_TC03(){
        Activity activity = new Activity(
                "Name", new Date(2017, 06, 05),
                new Date(2017, 06, 06),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);
            assert true;
        } catch (RepositoryException e) {
            assert false;
        }
    }


    @Test
    public void addActivity_TC04(){
        Activity activity = new Activity(
                "Name", new Date(2017, 06, 5),
                new Date(2017, 06, 1),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);
            assert true;
        } catch (RepositoryException e) {
            assert false;
        }

    }

    @Test
    public void addActivity_TC05(){
        try {
            emptyRepository();
            repositoryActivity = new RepositoryActivityFile(repositoryContact, repositoryUser, "bin/files/testActivity.txt");

        } catch (Exception e) {
            e.printStackTrace();
        }
        Activity activity = new Activity(
                "Name", new Date(2017, 06, 3),
                new Date(2017, 06, 4),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);
            assert true;
        } catch (RepositoryException e) {
            assert false;
        }

    }









}
