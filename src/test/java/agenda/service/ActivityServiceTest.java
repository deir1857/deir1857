package agenda.service;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Activity;
import agenda.model.base.User;
import agenda.model.validator.ActivityValidator;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.IRepositoryActivity;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by elisei on 07.05.2018.
 */
public class ActivityServiceTest {

    private IRepositoryActivity repositoryActivity;
    private IRepositoryContact repositoryContact ;
    private IRepositoryUser repositoryUser;
    private ActivityService activityService;
    private User user;



    public void emptyRepository(){
        try{

            File file = new File("bin/files/testActivity.txt");
            if(file.exists())
                file.delete();
            file.createNewFile();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Before
    public void setUp() throws Exception{
        emptyRepository();

        repositoryUser = new RepositoryUserFile("bin/files/testUser.txt");
        repositoryContact = new RepositoryContactFile(repositoryUser, "bin/files/testContact.txt");
        repositoryActivity = new RepositoryActivityFile(repositoryContact, repositoryUser, "bin/files/testActivity.txt");
        activityService = new ActivityService(repositoryActivity, new ActivityValidator());
        user = repositoryUser.getByUsername("username1");

        Activity activity = new Activity(
                "Name", new Date(2017, 06, 03),
                new Date(2017, 06, 04),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            repositoryActivity.addActivity(activity);

        } catch (RepositoryException e) {
            assert false;
        }
    }


    @Test
    public void generateReportValidCase(){
        List<Activity> result;
        Activity found ;
        Activity activity = new Activity(
                "Name", new Date(2017, 06, 03),
                new Date(2017, 06, 04),
                new ArrayList<>(),
                "Description",
                user,
                "Location"
        );

        try {
            result =  activityService.activitiesOnDate(
                    user,
                    new Date(2017, 06, 01),
                    new Date(2017, 06, 06)
            );
            assert result.size() == 1;
            found = result.get(0);
            assert found.getUser().getUsername().equals(activity.getUser().getUsername());
            assert found.getEnd().equals(activity.getEnd());
            assert found.getStart().equals(activity.getStart());
            assert found.getLocation().equals(activity.getLocation());
            assert found.getDescription().equals(activity.getDescription());
            assert found.getName().equals(activity.getName());
        } catch (InvalidFormatException e) {
            assert false;
        }

    }


    @Test
    public void generateReportInvalidCase(){
        try{
            activityService.activitiesOnDate(
                    user,
                    new Date(2017, 06, 03),
                    new Date(2017, 06, 02)
            );
            assert false;
        } catch (InvalidFormatException e) {
            assert true;
        }
    }
}
