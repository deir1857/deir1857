package agenda.service;

import agenda.exceptions.InvalidFormatException;
import agenda.exceptions.RepositoryException;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.validator.ContactValidator;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.IRepositoryContact;
import agenda.repository.interfaces.IRepositoryUser;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by elisei on 02.04.2018.
 */
public class ContactServiceTest {

    private IRepositoryContact repositoryContact;
    private IRepositoryUser repositoryUser;
    private User user;
    private ContactService contactService;

    @Before
    public void setUp() throws Exception{
        repositoryUser = new RepositoryUserFile("bin/files/testUser.txt");
        repositoryContact = new RepositoryContactFile(repositoryUser, "bin/files/testContact.txt");
        user = repositoryUser.getByUsername("username1");
        contactService = new ContactService(repositoryContact, new ContactValidator());
    }



    @Test
    public void addContact() throws Exception {

    }


    @Test
    public void addContactTC1_ECP(){
        try {
            contactService.addContact("Elisei Vasile",
                        "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                        "+40752313122",
                        "Elisei",
                        user
                        );
            assertTrue(true);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addContactTC3_ECP(){
        try {
            contactService.addContact("Elisei",
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "Elisei",
                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid name"));
        }
    }

    @Test
    public void addContactTC4_ECP(){
        try {
            contactService.addContact("Elisei Vasile"	,
                    "Strada S-- nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "Elisei",
                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid address"));
        }
    }

    @Test
    public void addContactTC8_ECP(){
        try {
            contactService.addContact("Elisei Vasile"	,
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania"	,
                    "+407523131223",
                    "Elisei",
                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
        }
    }

    @Test
    public void addContactTC10_ECP(){
        try {
            contactService.addContact("Elisei Vasile"	,
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania"	,
                    "+40752313122",
                    "Eliseiasdfghddfhfdhhasgdgasgjgjadsjgadsgjhasdhjsadjjdsajhsdajhadshj",
                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid email"));
        }
    }

    @Test
    public void addContactTC11_ECP(){
        try {
            contactService.addContact("Elisei Vasile"	,
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "",
                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid email"));
        }
    }


    @Test
    public void addContactTC3_BVA(){
        try {
            contactService.addContact("A A",
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "Elisei",
                    user
            );
            assertTrue(true);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
    }


    @Test
    public void addContactTC4_BVA(){
        try {
            contactService.addContact(" ",
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "Elisei",

                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid name"));
        }
    }


    @Test
    public void addContactTC5_BVA(){
        try {
            contactService.addContact("AAAAAAAAAAAAAAAAAAAAAAAA"  + " " + "AAAAAAAAAAAAAAAAAAAAAAAAA",
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+40752313122",
                    "Elisei",
                    user
            );
            assertTrue(true);
        } catch (RepositoryException | InvalidFormatException e) {
            assertTrue(false);
        }
    }


    @Test
    public void addContactTC15_BVA(){
        try {
            contactService.addContact("Elisei Vasile"	,
                    "Strada Vidin nr. 68, Oras Chisinau, Judet Prahivita, Tara Romania",
                    "+407",
                    "Elisei",
                    user
            );
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertTrue(e.getMessage().equals("Invalid format"));
            assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
        }
    }



}